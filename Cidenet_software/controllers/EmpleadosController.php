<?php
require_once "views/EmpleadosView.php";
require_once "models/EmpleadosModel.php";

    class EmpleadosController
    {

        //funcion para llevar los datos de la base de datos a la vista y mostrarlos en una tabla
        function paginateEmpleados()
        {
            $Connection = new Connection('root');
            $EmpleadosModel = new EmpleadosModel($Connection);

            $array_empleado = $EmpleadosModel->paginateEmpleados();

            $EmpleadosView = new EmpleadosView();
            $EmpleadosView->paginateEmpleados($array_empleado);
        }

        //funcion para filtrar empleados
        function busqueda()
        {
            $Connection=new Connection('root');
        
            $EmpleadosModel = new EmpleadosModel($Connection);
            
            $EmpleadosView = new EmpleadosView();
            
            
            $busqueda = $_POST['busqueda'];
            $filtro = $_POST['filtro'];
   
            
            if($busqueda=='')
            {
                $response=["message"=>'debe ingresar un dato para filtrar'];
                exit(json_encode($response));
            }

            if($filtro==1)
            {
                $array_empleado=$EmpleadosModel->buscarXnombre1($busqueda);
            }
            if($filtro==2)
            {
                $array_empleado=$EmpleadosModel->buscarXnombre2($busqueda);
            }
            if($filtro==3)
            {
                $array_empleado=$EmpleadosModel->buscarXapellido1($busqueda);
            }
            if($filtro==4)
            {
                $array_empleado=$EmpleadosModel->buscarXapellido2($busqueda);
            }
            if($filtro==5)
            {
                $array_empleado=$EmpleadosModel->buscarXdocumento($busqueda);
            }
            if($filtro==6)
            {
                $array_empleado=$EmpleadosModel->buscarXpais($busqueda);
            }
            if($filtro==7)
            {
                $array_empleado=$EmpleadosModel->buscarXcorreo($busqueda);
            }



            

            if($array_empleado==null)
            {
                $response=["message"=>'no se encontro el empleado con el criterio de busqueda ingresado'];
                exit(json_encode($response));
            }else{
                $EmpleadosView->paginateEmpleados($array_empleado);
            }
            
             
        }
      

       

        

       
        //funcion para mostrar en la vista el formulario de registro de empleados
        function EmpleadosRegistro()
        {
            $Connection=new Connection('root');        
             $EmpleadosModel = new EmpleadosModel($Connection);
            
            // $orde_token=$_POST['id'];
            $array_Pais = $EmpleadosModel->queryPais();
            $array_Areas = $EmpleadosModel->queryAreas();
            $array_Documento = $EmpleadosModel->queryDocumento();
            
            $EmpleadosView = new EmpleadosView();
            $EmpleadosView->EmpleadosRegistro($array_Pais, $array_Areas, $array_Documento);
        }


        //funcion para insertar los datos del formulario den la base de daros
        function EmpleadosInsert()
        {
            $Connection=new Connection('root');
        
            $EmpleadosModel = new EmpleadosModel($Connection);
            
            $EmpleadosView = new EmpleadosView();
            
            $nombre1 = $_POST['name1'];
            $nombre2 = $_POST['name2'];
            $apellido1 = $_POST['apellido1'];
            $apellido2 = $_POST['apellido2'];
            $numDocumento = $_POST['numDocument'];
            $estado = "activo";
            $id_pais = $_POST['pais_id'];
            $id_area = $_POST['area_id'];
            $id_documento = $_POST['document_id'];
            $fecha_ingreso = $_POST['fecha_ingreso'];

            date_default_timezone_set('America/Mexico_City');
            $time = time();
            $fecha_registro=date('Y-m-d',$time);
            $hora_registro=date('H:i',$time);

            
            $array_empleadosID=$EmpleadosModel->selectId();

            if($array_empleadosID==null){
                $emp_id=1;
            }else{
                $emp_id=$array_empleadosID[0]->id;

            $emp_id=$emp_id+1;
            }

            
            
            

            

            if ($id_pais == 1){
                $correo=$nombre1.".".$apellido1.".".$emp_id."@cidenet.com.co";
            }
            if ($id_pais == 2){
                $correo=$nombre1.".".$apellido1.".".$emp_id."@cidenet.com.us";
            }

            if($nombre1=='')
            {
                $response=["message"=>'el nombre es obligatorio'];
                exit(json_encode($response));
            }

            
            
            if($apellido1=='')
            {
                $response=["message"=>'el apellido es obligatorio'];
                exit(json_encode($response));
            }

            if($numDocumento=='')
            {
                $response=["message"=>'el numero de documento es obligatorio'];
                exit(json_encode($response));
            }

            if($id_pais=='')
            {
                $response=["message"=>'seleccione un pais'];
                exit(json_encode($response));
            }
            if($id_area=='')
            {
                $response=["message"=>'seleccione un area'];
                exit(json_encode($response));
            }
            if($id_documento=='')
            {
                $response=["message"=>'seleccione el tipo de documento'];
                exit(json_encode($response));
            }

            if(strlen($nombre1)>20){$response=["message"=>'el nombre no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}

            if(strlen($nombre2)>50){$response=["message"=>'otros nombres no pueden ser de mas de 50 caracteres'];
                exit(json_encode($response));}

            if(strlen($apellido1)>20){$response=["message"=>'el apellido no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}

            if(strlen($apellido2)>20){$response=["message"=>'el apellido no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}

            if(strlen($numDocumento)>20){$response=["message"=>'el numero de documento no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}

            $array_document=$EmpleadosModel->duplicateDocumento($numDocumento);

            if($array_document)
            {
                $array_message=['message'=>'ya existe este documento'];
                exit(json_encode($array_message));
            }
            
            


           
            $EmpleadosModel->EmpleadosInsert($nombre1,$nombre2,$apellido1,$apellido2,$id_area,$id_documento,$numDocumento,$estado,$id_pais,$correo,$fecha_ingreso,$fecha_registro,$hora_registro);

            $array_empleado=$EmpleadosModel->paginateEmpleados();
            
            $EmpleadosView->paginateEmpleados($array_empleado); 
        }

       
        //funcion para actualizar los datos en la base de datos
        function updateEmpleados()
        {
            

            $Connection=new Connection('root');
            
            $EmpleadosModel=new EmpleadosModel($Connection);

            $EmpleadosView=new EmpleadosView();

            $emp_id=$_POST['id'];
            
            $nombre1 = $_POST['nombre1'];
            $nombre2 = $_POST['nombre2'];
            $apellido1 = $_POST['apellido1'];
            $apellido2 = $_POST['apellido2'];
            $numDocumento = $_POST['numDocumento'];
            $estado = "activo";
            $id_pais = $_POST['id_pais'];
            $id_area = $_POST['id_area'];
            $id_documento = $_POST['id_Documento'];
            $fecha_ingreso = $_POST['fecha_ingreso'];

            date_default_timezone_set('America/Mexico_City');
            $time = time();
            $fecha_registro=date('Y-m-d',$time);
            $hora_registro=date('H:i',$time);

            if ($id_pais == 1){
                $correo=$nombre1.".".$apellido1.".".$emp_id."@cidenet.com.co";
            }
            if ($id_pais == 2){
                $correo=$nombre1.".".$apellido1.".".$emp_id."@cidenet.com.us";
            }

            
            //validaciones
           if($nombre1=='')
            {
                $response=["message"=>'el nombre es obligatorio'];
                exit(json_encode($response));
            }

            
            
            if($apellido1=='')
            {
                $response=["message"=>'el apellido es obligatorio'];
                exit(json_encode($response));
            }

            if($numDocumento=='')
            {
                $response=["message"=>'el numero de documento es obligatorio'];
                exit(json_encode($response));
            }

            if($id_pais=='')
            {
                $response=["message"=>'seleccione un pais'];
                exit(json_encode($response));
            }
            if($id_area=='')
            {
                $response=["message"=>'seleccione un area'];
                exit(json_encode($response));
            }
            if($id_documento=='')
            {
                $response=["message"=>'seleccione el tipo de documento'];
                exit(json_encode($response));
            }

            if(strlen($nombre1)>20){$response=["message"=>'el nombre no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}

            if(strlen($nombre2)>50){$response=["message"=>'otros nombres no pueden ser de mas de 50 caracteres'];
                exit(json_encode($response));}

            if(strlen($apellido1)>20){$response=["message"=>'el apellido no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}

            if(strlen($apellido2)>20){$response=["message"=>'el apellido no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}

            if(strlen($numDocumento)>20){$response=["message"=>'el numero de documento no puede ser de mas de 20 caracteres'];
                exit(json_encode($response));}


            //validacion para saber si el documento ya existe en la base de datos
            $array_document=$EmpleadosModel->duplicateDocumentoUpdate($numDocumento,$emp_id);

            if($array_document)
            {
                $array_message=['message'=>'ya existe este documento'];
                exit(json_encode($array_message));
            }
            

            

            
            
            $EmpleadosModel->updateEmpleados($nombre1,$nombre2,$apellido1,$apellido2,$id_area,$id_documento,$numDocumento,$correo,$estado,$id_pais,$fecha_ingreso,$fecha_registro,$hora_registro,$emp_id);

            $array_empleado=$EmpleadosModel->paginateEmpleados();

            $EmpleadosView->paginateEmpleados($array_empleado);   
        }


        //funcion para mostrar el formulario de edicion de empleados en la vista
        function showEmpleados()
        {
            $Connection=new Connection('root');        
            $EmpleadosModel = new EmpleadosModel($Connection);

            $emp_id=$_POST['id'];
            
            
            $array_Pais = $EmpleadosModel->queryPais();
            $array_Areas = $EmpleadosModel->queryAreas();
            $array_Documento = $EmpleadosModel->queryDocumento();

            $array_empleado=$EmpleadosModel->selectEmpleado(['field'=>'id','value'=>$emp_id]);
            
            $EmpleadosView = new EmpleadosView();
            $EmpleadosView->showEmpleados($array_Pais, $array_Areas, $array_Documento, $array_empleado);
        }

        //funcion para eliminar empleados de la base de datos
        function DeleteEmpleados()
        {
            

            $Connection=new Connection('root');
            
            $EmpleadosModel=new EmpleadosModel($Connection);

            $EmpleadosView=new EmpleadosView();

            $emp_id=$_POST['id'];

            $array_empleadosID=$EmpleadosModel->selectId();

            
            

            
            
            $EmpleadosModel->DeleteEmpleados($emp_id);

            $array_empleado=$EmpleadosModel->paginateEmpleados();

            $EmpleadosView->paginateEmpleados($array_empleado);   
        }

        

   
    }

?>