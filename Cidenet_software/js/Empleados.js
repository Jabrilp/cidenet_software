class EmpleadosJs
{

  

   
    //funcion javascripy para enviar los datos que vienen del formulario al controlador
    EmpleadosInsert()
    {
        var object=new FormData(document.querySelector('#form_assign_task'));
        
        fetch('EmpleadosController/EmpleadosInsert',{
            method:'POST',
            body:object
        })
        .then((resp) => resp.text())
        .then(function(response)
        {
            try
            {
                object =JSON.parse(response);
                toastr.error(object.message);
            }
            catch(error)
            {
                document.querySelector('#content').innerHTML=response;
                toastr.success('¡Empleado registrado exitosamente!');
            }
        })
        .catch(function(error) {
          console.log(error);
        }); 
    }

    //funcion javascript para llamar la funcion de mostrar el formulario de registro del controlador a la vista
    EmpleadosRegistro()
    {
        var object=new FormData();
        
        
        fetch('EmpleadosController/EmpleadosRegistro',{
            method:'POST',
            body:object
        })
        .then((resp) => resp.text())
        .then(function(response)
        {
            $('#my_modal').modal('show');

            document.querySelector('#modal_title').innerHTML="Agregar Empleado";

            document.querySelector('#modal_content').innerHTML=response;
        })
        .catch(function(error) {
          console.log(error);
        }); 
    }





    //funcion javascript que llama la funcion del controlador para actualizar los empleados en la base de datos
    UpdateEmpleados()
    {

        
        
        var object=new FormData(document.querySelector('#form_assign_task'));
        
        fetch('EmpleadosController/updateEmpleados',{
            method:'POST',
            body:object
        })
        .then((resp) => resp.text())
        .then(function(response)
        {
            try
            {
                object =JSON.parse(response);
                toastr.error(object.message);
            }
            catch(error)
            {
                document.querySelector('#content').innerHTML=response;
                toastr.success('¡Empleado actualizado exitosamente!');
            }
        })
        .catch(function(error) {
          console.log(error);
        }); 
    }


    //funcion javascript para llamar la funcion de edicion de usuarios del controlador a la vista
    showEmpleados(id)
    { 
        var object=new FormData();

        object.append('id', id);

        
        
        $('#my_modal').modal('show');

        document.querySelector('#modal_title').innerHTML="Actualizar datos del empleado";

        fetch('EmpleadosController/showEmpleados',{
            method: 'POST',
            body: object
        })
        .then((resp) => resp.text())
        .then(function(data)
        {
            document.querySelector('#modal_content').innerHTML=data;
        })
        .catch(function(error) {
          console.log(error);
        });
    }

    //funcion javascript para ir a la funcion del controlador que permite elminiar los empleados de la base de datos
    DeleteEmpleados(id)
    {

        if(confirm("¿Confirma su eliminación?")){
        
            var object=new FormData();

            object.append('id', id);
        
            fetch('EmpleadosController/DeleteEmpleados',{
                method:'POST',
                body:object
            })
            .then((resp) => resp.text())
            .then(function(response)
            {
                try
                {
                    object =JSON.parse(response);
                    toastr.error(object.message);
                }
                catch(error)
                {
                    document.querySelector('#content').innerHTML=response;
                    toastr.success('¡Empleado eliminado exitosamente!');
                }
            })
            .catch(function(error) {
              console.log(error);
            }); 
        
        }
        
        
    }


    //funcion javascript para ir a la funcion de busqueda del controlador que permite filtrar los empleados en la consulta
    busqueda()
    {

        
        
        var object=new FormData(document.querySelector('#form_assign_emps'));
        
        fetch('EmpleadosController/busqueda',{
            method:'POST',
            body:object
        })
        .then((resp) => resp.text())
        .then(function(response)
        {
            try
            {
                object =JSON.parse(response);
                toastr.error(object.message);
            }
            catch(error)
            {
                document.querySelector('#content').innerHTML=response;
                toastr.success('¡Busqueda exitosa!');
            }
        })
        .catch(function(error) {
          console.log(error);
        }); 
    }



//funcion javascript para ir a la funcion del controlador que permite consultar todos los datos que estan en la base de datos
    back()
    {
        var object=new FormData();
        
        fetch('EmpleadosController/paginateEmpleados',{
            method:'POST',
            body:object
        })
        .then((resp) => resp.text())
        .then(function(response)
        {
            $('#content').html(response);
        })
        .catch(function(error) {
          console.log(error);
        }); 
    }
}

var Emp=new EmpleadosJs();