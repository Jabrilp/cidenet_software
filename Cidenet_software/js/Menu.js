class MenuJs
{

    menu(route)
    {
        fetch(route)
        .then((resp) => resp.text())
        .then(function(response)
        {
            $('#content').html(response);
        })
        .catch(function(error) {
          console.log(error);
        });  
    }
}

var Menu=new MenuJs();