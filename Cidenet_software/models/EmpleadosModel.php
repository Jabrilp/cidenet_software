<?php

    
    class EmpleadosModel
    {

        private $Connection;

        function __construct($Connection)
        {
            $this->Connection = $Connection;
        }

        //funcion que hace la consulta sql para traer todos los datos de los empleados de la base de datos
        function paginateEmpleados()
        {
            $sql="SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id";

            $this->Connection->query($sql); 

            return $this->Connection->fetchAll();
        }

        ////funcion que hace la consulta sql para trar los datos de la base datos donde el primer nombre sea igual a la variable que viene como parametro del controlador
        function buscarXnombre1($busqueda)
        {
            $sql = "SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id WHERE primerNombre='$busqueda';";
            $this->Connection->query($sql);
            return $this->Connection->fetchAll();
        }

//funcion que hace la consulta sql para trar los datos de la base datos donde el segundo nombre sea igual a la variable que viene como parametro del controlador
        function buscarXnombre2($busqueda)
        {
            $sql = "SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id WHERE segundoNombre='$busqueda';";
            $this->Connection->query($sql);
            return $this->Connection->fetchAll();
        }

        //funcion que hace la consulta sql para trar los datos de la base datos donde el primer apellido sea igual a la variable que viene como parametro del controlador
        function buscarXapellido1($busqueda)
        {
            $sql = "SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id WHERE primerApellido='$busqueda';";
            $this->Connection->query($sql);
            return $this->Connection->fetchAll();
        }

        //funcion que hace la consulta sql para trar los datos de la base datos donde el segundo apellido sea igual a la variable que viene como parametro del controlador
        function buscarXapellido2($busqueda)
        {
            $sql = "SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id WHERE segundoApellido='$busqueda';";
            $this->Connection->query($sql);
            return $this->Connection->fetchAll();
        }


        //funcion que hace la consulta sql para trar los datos de la base datos donde el numero de documento sea igual a la variable que viene como parametro del controlador
        function buscarXdocumento($busqueda)
        {
            $sql = "SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id WHERE numIdentificacion='$busqueda';";
            $this->Connection->query($sql);
            return $this->Connection->fetchAll();
        }


        //funcion que hace la consulta sql para trar los datos de la base datos donde el pais sea igual a la variable que viene como parametro del controlador
        function buscarXpais($busqueda)
        {
            $sql = "SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id WHERE pais='$busqueda';";
            $this->Connection->query($sql);
            return $this->Connection->fetchAll();
        }


        //funcion que hace la consulta sql para trar los datos de la base datos donde el correo sea igual a la variable que viene como parametro del controlador
        function buscarXcorreo($busqueda)
        {
            $sql = "SELECT p.pais, 
            e.id, e.primerNombre, 
            e.segundoNombre, 
            e.primerApellido, 
            e.segundoApellido,
            e.numIdentificacion,
            e.correo, 
            e.estado,
            e.fecha_ingreso,
            e.fecha_registro,
            e.hora_registro,
            a.nombreArea
            FROM pais p INNER JOIN empleados e
            ON p.id=e.idPais
            INNER JOIN areas a
            ON e.idArea=a.id WHERE correo='$busqueda';";
            $this->Connection->query($sql);
            return $this->Connection->fetchAll();
        }

       


    
        //funcion que haace la consulta sql para traer todos los datos de la tabla pais
        function queryPais()
        {
            $sql="SELECT * FROM pais;";

            $this->Connection->query($sql);

            return $this->Connection->fetchAll();
        }


        //funcion que hace la consulta sql para traer todos los datos de la tabla areas
        function queryAreas()
        {
            $sql="SELECT * FROM areas;";

            $this->Connection->query($sql);

            return $this->Connection->fetchAll();
        }

        //funcion que hace la consulta sql para reaer todos los datos de la tabla tipos
        function queryDocumento()
        {
            $sql="SELECT * FROM tipos;";

            $this->Connection->query($sql);

            return $this->Connection->fetchAll();
        }

      
        //funcion que hace la consulta sql para insertar en la base de datos todos los datos que vienen como variables desde el controlador
        function EmpleadosInsert($nombre1,$nombre2,$apellido1,$apellido2,$id_area,$id_documento,$numDocumento,$estado,$id_pais,$correo,$fecha_ingreso,$fecha_registro,$hora_registro)
        {
            $sql="INSERT INTO empleados (primerNombre,segundoNombre,primerApellido,segundoApellido,idArea,idTipo,numIdentificacion,estado,idPais,correo,fecha_ingreso,fecha_registro,hora_registro) VALUES ('$nombre1','$nombre2','$apellido1','$apellido2','$id_area','$id_documento','$numDocumento','$estado','$id_pais','$correo','$fecha_ingreso','$fecha_registro','$hora_registro')";

            $this->Connection->query($sql);
        }


        //funcion que hace la consulta sql para insertar el cooreo en la base de datos 
        function insertCorreo($correo)
        {
            $sql="INSERT INTO empleados (correo) VALUES ('$correo')";

            $this->Connection->query($sql);
        }

      
        //funcion que hace la consulta sql para actualizar los datps de los empleados en la base de datos
        function updateEmpleados($nombre1,$nombre2,$apellido1,$apellido2,$id_area,$id_documento,$numDocumento,$correo,$estado,$id_pais,$fecha_ingreso,$fecha_registro,$hora_registro,$emp_id)
        {
            $sql="UPDATE empleados SET primerNombre='$nombre1', 
            segundoNombre='$nombre2', 
            primerApellido='$apellido1',
            segundoApellido='$apellido2',
            idArea='$id_area',
            idTipo='$id_documento',
            numIdentificacion='$numDocumento',
            correo='$correo',
            estado='$estado',
            idPais='$id_pais',
            fecha_ingreso='$fecha_ingreso',
            fecha_registro='$fecha_registro',
            hora_registro='$hora_registro'

            WHERE id='$emp_id'";

            $this->Connection->query($sql);
        }


        //funcion que hace la consulta sql para elmunuar un empleado de la base de datos
        function DeleteEmpleados($emp_id)
        {
            $sql="DELETE FROM empleados WHERE id='$emp_id'";

            $this->Connection->query($sql);
        }
//funcion sql para traer un empleado de la base de datps
    function selectEmpleado($array)
    {
        $field=$array['field'];
        $value=$array['value'];

        $sql="SELECT * FROM empleados WHERE $field='$value'";

        $this->Connection->query($sql);

        return $this->Connection->fetchAll();
    }


    //funcion que hace la consulta sql para traer los datos del empleado que tenga el id de mayor tamaño
    function selectId()
    {
        

        $sql="SELECT * FROM empleados WHERE id = (SELECT MAX(id) FROM empleados)";

        $this->Connection->query($sql);

        return $this->Connection->fetchAll();
    }

    //funcion que hace la consulta para traer el empleado en el que el documendo sea igual al que vine como parametro del controlador, esto para poder validarlo posteriormente
    function duplicateDocumento($numDocumento)
    {
        
            $sql="SELECT * FROM empleados WHERE numIdentificacion='$numDocumento'";
        
        

        $this->Connection->query($sql);

        return $this->Connection->fetchAll();
    }

    
    function duplicateDocumentoUpdate($numDocumento,$emp_id)
    {
        
            $sql="SELECT * FROM empleados WHERE numIdentificacion='$numDocumento' AND id != '$emp_id'";
        
        

        $this->Connection->query($sql);

        return $this->Connection->fetchAll();
    }

    

      

       

        

        
    }
?>