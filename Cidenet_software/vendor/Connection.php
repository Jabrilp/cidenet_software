<?php
require_once "setting_connection.php";

class Connection
{
    private $link;
    private $result;
    
    function __construct($option)
    {
        $ip=IP;
        $data_base=DATA_BASE;
        
        if($option=='root')
        {
            $user=USER;
            $password=PASSWORD;
        }
       
        else
        {
            exit('Falta la opción de conexión');
        }

        try
        {
            $this->link=new PDO("mysql:host=$ip;dbname=$data_base",$user,$password);
        }
        catch(PDOException $e)
        {
            exit($e->getMessage());
        }
    }

    function query($sql)
    {
        $this->result=$this->link->query($sql) or exit('Consulta mal estructurada');
    }

    function fetchAll()
    {
        return $this->result->fetchAll(PDO::FETCH_OBJ);
    }
}
?>