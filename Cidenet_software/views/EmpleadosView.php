<?php
    //clase EmpleadosView donde se van a visualizar todos los metodos de consultar, insertar, actualizar y eliminar
    class EmpleadosView
    {   
        //funcion PaginateEmpleados: muestra todos los datos registrados en la base de datos en una tabla, ademas del filtro de busqueda y los botones necesarios para realizar las acciones de agregar empleados, actualizarlos y eliminiarlos de la base de datos
        function paginateEmpleados($array_empleado)
        {
            ?>
            <div class="card">
              <div class="card-header">
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        <h3 class="card-title"><b>Administrador de empleados</b></h3>
                    </div>
                </div>
                <form id="form_assign_emps">
                    <div class="form-group">
                        <label>Criterio de busqueda</label>
                        <input type="text" class="form-control" id="busqueda" name="busqueda">
                        <div class="form-group">
                            <label for="people_peop_id">Filtrar por:</label>
                            <select class="custom-select" name="filtro" id="fitro">

                            <option selected value="1">primer nombre</option>
                            <option selected value="2">segundo nombre</option>
                            <option selected value="3">primer apellido</option>
                            <option selected value="4">segundo apellido</option>
                            <option selected value="5">numero de identificacion</option>
                            <option selected value="6">pais de empleo</option>
                            <option selected value="7">correo</option>
                            
                            </select>
                        </div>
                        <button type="button" class="btn btn-info float-left" onclick="Emp.busqueda()">
                    <i class="fas fa-save"></i> buscar
                    </button>
                    <button style="margin-left: 10px" type="button" class="btn btn-info float-left" onclick="Emp.back()">
                    <i class="fas fa-save"></i> mostrar todo
                    </button>

                    </div>

                    
                </form>
                <div class="row">
                    <div class="col col-md-12">
                        <button type="button" class="btn btn-info float-right" onclick="Emp.EmpleadosRegistro()">
                        <i class="nav-icon fas fa-plus-circle"></i> &nbsp;Agregar Empleado
                        </button>
                    </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead class="bg-info">
                    <tr>
                      <th style="width: 10px">#</th>
                      <th style="width: 100px">nombres</th>
                      <th style="width: 100px">apellidos</th>
                      <th style="width: 100px">area</th>
                      <th style="width: 100px">documento</th>
                      <th style="width: 100px">correo</th>
                      <th style="width: 100px">estado</th>
                      <th style="width: 100px">pais</th>
                      <th style="width: 100px">fecha de ingreso</th>
                      <th style="width: 100px">fecha de registro</th>

                      <th style="width: 50px; text-align:center;">accion</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php                            
                        foreach ($array_empleado as $emps)
                        {
                            $emp_id=$emps->id;
                            $nombre=$emps->primerNombre;
                            $nombre2=$emps->segundoNombre;
                            $apellido=$emps->primerApellido;
                            $apellido2=$emps->segundoApellido;
                            $area=$emps->nombreArea;
                            $pais=$emps->pais;
                            $correo=$emps->correo;
                            $estado=$emps->estado;
                            $documento=$emps->numIdentificacion;
                            $fecha_ingreso=$emps->fecha_ingreso;
                            $fecha_registro=$emps->fecha_registro;
                            $hora_registro=$emps->hora_registro;
                            
                            
                            ?>
                            <tr>
                                <td><?php echo $emp_id?></td>
                                <td><?php echo $nombre." ".$nombre2?></td>
                                <td><?php echo $apellido." ".$apellido2?></td>
                                <td><?php echo $area?></td>
                                <td><?php echo $documento?></td>
                                <td><?php echo $correo?></td>
                                <td><?php echo $estado?></td>
                                <td><?php echo $pais?></td>
                                <td><?php echo $fecha_ingreso?></td>
                                <td><?php echo $fecha_registro." ".$hora_registro?></td>
                                
                                <td style="text-align:center;">
                                <i class="fas fa-edit" onclick="Emp.showEmpleados('<?php echo $emp_id;?>');"></i>
                                <i class="fas fa-minus-circle" onclick="Emp.DeleteEmpleados('<?php echo $emp_id;?>');"></i>
                                </td>
                            </tr>
                            <?php
                        
                        }
                    ?>                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <?php
        }


        
        //funcion para mostrar el formulario de registro de usuarios
        function EmpleadosRegistro($array_Pais, $array_Areas, $array_Documento)
        {
            ?>
            <div class="card">
                <div class="card-body">
                    <form id="form_assign_task">
                        

                        <div class="form-group">
                            <label>primer nombre</label>
                            <input type="text" class="form-control" id="name1" name="name1">
                        </div>
                        <div class="form-group">
                            <label>segundo nombre</label>
                            <input type="text" class="form-control" id="name2" name="name2">
                        </div>
                        <div class="form-group">
                            <label>primer apellido</label>
                            <input type="text" class="form-control" id="apellido1" name="apellido1">
                        </div>
                        <div class="form-group">
                            <label>segundo apellido</label>
                            <input type="text" class="form-control" id="apellido2" name="apellido2">
                        </div>
                        <div class="form-group">
                            <label for="people_peop_id">tipo de documento</label>
                            <select class="custom-select" name="document_id" id="document_id">
                            <option selected value="">Seleccione una persona...</option>
                            <?php
                                foreach($array_Documento AS $doc)
                                {
                                    $doc_id = $doc->id;
                                    $doc_tipo = $doc->tipo;
                                    
                                    ?>
                                    <option value=<?php echo $doc_id?>><?php echo $doc_tipo;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>numero de documento</label>
                            <input type="text" class="form-control" id="numDocument" name="numDocument">
                        </div>
                        

                        <div class="form-group">
                            <label for="people_peop_id">Paises</label>
                            <select class="custom-select" name="pais_id" id="pais_id">
                            <option selected value="">Seleccione un pais...</option>
                            <?php
                                foreach($array_Pais AS $pais)
                                {
                                    $pais_id = $pais->id;
                                    $pais_name = $pais->pais;
                                    
                                    ?>
                                    <option value=<?php echo $pais_id?>><?php echo $pais_name;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="people_peop_id">areas</label>
                            <select class="custom-select" name="area_id" id="area_id">
                            <option selected value="">Seleccione un area...</option>
                            <?php
                                foreach($array_Areas AS $areas)
                                {
                                    $areas_id = $areas->id;
                                    $area_name = $areas->nombreArea;
                                    
                                    ?>
                                    <option value=<?php echo $areas_id?>><?php echo $area_name;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="peta_date">Fecha de ingreso</label>
                            <input type="date" class="form-control" name="fecha_ingreso" id="fecha_ingreso">
                        </div>

                        

                        <button type="button" class="btn btn-info float-right" onclick="Emp.EmpleadosInsert()">
                        <i class="nav-icon fas fa-save"></i> registrar
                        </button>
                    </form>
                </div>
            </div>
            <?php
        }

        //funcion para mostrar el formulario de edicion de empleados
        function showEmpleados($array_Pais, $array_Areas, $array_Documento, $array_empleado)
        {

            $emp_id=$array_empleado[0]->id;
            $nombre1=$array_empleado[0]->primerNombre;
            $nombre2=$array_empleado[0]->segundoNombre;
            $apellido1=$array_empleado[0]->primerApellido;
            $apellido2=$array_empleado[0]->segundoApellido;
            
            
            $numDocumento=$array_empleado[0]->numIdentificacion;
            
            $estado=$array_empleado[0]->estado;
            
            ?>

            
            <div class="card">
                <div class="card-body">
                    <form id="form_assign_task">
                        

                        <div class="form-group">
                            <label>primer nombre</label>
                            <input type="text" class="form-control" id="nombre1" name="nombre1" value="<?php echo $nombre1;?>">
                        </div>
                        <div class="form-group">
                            <label>segundo nombre</label>
                            <input type="text" class="form-control" id="nombre2" name="nombre2" value="<?php echo $nombre2;?>">
                        </div>
                        <div class="form-group">
                            <label>primer apellido</label>
                            <input type="text" class="form-control" id="apellido1" name="apellido1" value="<?php echo $apellido1;?>">
                        </div>
                        <div class="form-group">
                            <label>segundo apellido</label>
                            <input type="text" class="form-control" id="apellido2" name="apellido2" value="<?php echo $apellido2;?>">
                        </div>
                        <div class="form-group">
                            <label for="people_peop_id">tipo de documento</label>
                            <select class="custom-select" name="id_Documento" id="id_Documento">
                            <option selected value="">Seleccione un tipo</option>
                            <?php
                                foreach($array_Documento AS $document)
                                {
                                    $document_id = $document->id;
                                    $tipo = $document->tipo;
                                    
                                    ?>
                                    <option value=<?php echo $document_id?>><?php echo $tipo;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>numero de documento</label>
                            <input type="numnber" class="form-control" id="numDocumento" name="numDocumento" value="<?php echo $numDocumento;?>">
                        </div>
                        <div class="form-group">
                            <label for="people_peop_id">Pais donde va a trabajar</label>
                            <select class="custom-select" name="id_pais" id="id_pais">
                            <option selected value="">Seleccione un Pais</option>
                            <?php
                                foreach($array_Pais AS $pais)
                                {
                                    $pais_id = $pais->id;
                                    $pais_name = $pais->pais;

                                    
                                    
                                    ?>
                                    <option value=<?php echo $pais_id?>><?php echo $pais_name;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="people_peop_id">Area de trabajo</label>
                            <select class="custom-select" name="id_area" id="id_area">
                            <option selected value="">Seleccione una persona...</option>
                            <?php
                                foreach($array_Areas AS $areas)
                                {
                                    $area_id = $areas->id;
                                    $area_name = $areas->nombreArea;
                                    
                                    ?>
                                    <option value=<?php echo $area_id?>><?php echo $area_name;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="peta_date">Fecha de ingreso</label>
                            <input type="date" class="form-control" name="fecha_ingreso" id="fecha_ingreso">
                        </div>

                        <input type="hidden" id="id" name="id" value="<?php echo $emp_id;?>">
                        

                        <button type="button" class="btn btn-info float-right" onclick="Emp.UpdateEmpleados()">
                        <i class="nav-icon fas fa-save"></i> Actualizar
                        </button>
                    </form>
                </div>
            </div>
            <?php
        }



    
    }

?>